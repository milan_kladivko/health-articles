#!/usr/bin/python3

import sys  # program arguments
import os   # directory listing
import re   # filtering files, parsing text



def main(argv):

    print('\nParsing custom-format text files into HTML')

    if len(argv)>=1 and argv[0] == '-h':
        # Print help and exit
        print("""
Usage: 
  python3 parse.py [html_output_dir [text_input_dir]]

Defaults: 
  html_output_dir = ./html/
  text_input_dir  = ./

The parser takes every file in input directory with the `.txt`
extension and parses it. The output files are put in the
output directory. 
        """)
        exit(1)

    # Program directories
    HTML_DIR = 'html/'
    TEXT_IN_DIR = './'
    HTML_OUT_DIR = './' + HTML_DIR
    if len(argv) > 0:
        HTML_OUT_DIR = argv[0]
    if len(argv) > 1:
        TEXT_IN_DIR = argv[1]

    # Verify input text files directory
    if not dir_is_valid(TEXT_IN_DIR): exit(1)

    # Make the directory tree if dirs don't exist
    make_dirnest_if_necessary(HTML_OUT_DIR)
    
    print(' text files from:', TEXT_IN_DIR)
    print(' extracting to:  ', HTML_OUT_DIR)

    # list the txt input directory, get files
    dir_list = os.listdir(TEXT_IN_DIR)
    is_infile = re.compile(r'(.*)\.txt$')
    infiles= filter(
        lambda f: is_infile.match(f), dir_list)

    # Go through all the matching files...
    print('Processing files:')
    for infile in infiles:
        
        # Read their contents
        text_path_name = TEXT_IN_DIR + infile
        print('\n ', text_path_name)
        text = open(text_path_name, 'r')
        text_lines = text.readlines()
        text.close()

        # Send them to get processed into HTML text
        html_lines = process(text_lines)

        # Write it into the html file
        outfile = is_infile.match(infile).group(1)
        # \- extract filename
        html_path_name = HTML_OUT_DIR + outfile + '.html'
        html = open(html_path_name, 'w')
        for line in html_lines:
            # print('.', end='')
            html.write(line + '\n') 
        html.close()
        print('    ->', html_path_name)
        



def process(file_lines):

    # Parser data
    CONVERSIONS = [
        # WARNING The order matters! 
        {
            'type': 'less-than',
            'regex': r'<',
            'html': r'&lt;'
        }, {
            'type': 'greater-than',
            'regex': r'>',
            'html': r'&gt;'
        },
        {
            'type': 'italics',
            'regex': r'//(.*?)//',
            'html': r'<i>\1</i>'
        }, {
            'type': 'bold',
            'regex': r'\$\$(.*?)\$\$',
            'html': r'<b>\1</b>'
        }, {
            'type': 'link',
            'regex': r'`(.*?)`\{(.*?)\}',
            'html': r'<a href="\2">\1</a>'
        }
    ]
    HEADINGS = {
        'regex': r'^ *### (.*)',
        'html': r'<h1>\1</h1>'
    }
    CLASSING = {
        'regex': r'^\[(.*?)\]$',
        'html': r' class="\1"'
    }
    LISTING = {
        'regex': r' *-- *(.*)',
        'html': r'  <li>\1</li>'
    }
    COMMENT_CHAR = '%'
    RAW_HTML = {
        'start': ' *<<<',
        'end': ' *>>>',
        'line': '^~~ (.*)'
    }

    # Variables
    html_lines = []

    prev_line_was_empty = True  # mark as true for first paragraph
    opened_paragraph = False
    opened_list = False
    opened_raw = False

    
    # Parse line by line
    for line in file_lines:
        
        # strip whitespace from right side (preserve indent)
        line = line.rstrip()
        this_is_empty_line = (len(line) == 0)
        indent = re.findall('^ *', line)[0]

        
        # ignore repeating empty lines
        if this_is_empty_line and prev_line_was_empty:
            continue
        # ignore comment lines
        if line.startswith(COMMENT_CHAR):
            continue


        
        # Ignore parsing on raw html in lines
        line_raw = re.match(RAW_HTML['line'], line)
        if line_raw:
            html_lines.append(line_raw.group(1))
            continue  # skip lines beginning with suffix
        
        if (opened_raw):
            if re.match(RAW_HTML['end'], line):
                opened_raw = False
                continue
            html_lines.append(line)
            continue  # skip parsing this line, even this ind line
        else:
            if re.match(RAW_HTML['start'], line):
                opened_raw = True
                continue
        

        heading_match = re.match(HEADINGS['regex'], line)
        if heading_match:
            heading_text = heading_match.expand(HEADINGS['html'])
            html_lines.append('\n\n' + heading_text + '\n\n')
            continue  # skip any further parsing of headings
            

        # *** Start a block
        
        class_match = re.match(CLASSING['regex'], line)
        list_match = re.match(LISTING['regex'], line)
        
        # paragraph open - on a text line following an empty one
        if (not this_is_empty_line) and prev_line_was_empty \
           and (not opened_paragraph):

            opened_paragraph = True

            # classing paragraphs + adding the open par. tag
            if class_match:  # no match -> None

                # write the opening tag
                class_attr = class_match.expand(
                    CLASSING['html'])
                html_lines.append('<p'+ class_attr +'>')
                
                # act as if this line was text
                # and prevent accidental double-tagging
                prev_line_was_empty = False
                # don't write the line itself, skip to next line
                continue
                
            else:
                html_lines.append('<p>')

        
        # list opening
        if list_match:
            
            # open the list if start of block
            if (not opened_list):
                opened_list = True
                html_lines.append('<ul>')

            # sub the current line as list item
            line = list_match.expand(LISTING['html'])


        # in-block formatting and flourishes
        if (not this_is_empty_line):
            # use regex conversions to parse in-text tagging
            # must be after paragraphing to avoid misrepresentation
            # ...these are just in-block formatting flourishes
            for CONV in CONVERSIONS:
                newline = re.sub(CONV['regex'], CONV['html'], line)
                line = newline

        
        # tag close - before first empty line after text block
        # WARNING watch that tag order, dont mix endings
        if this_is_empty_line:
            if opened_list:
                opened_list = False
                html_lines.append('</ul>')
            if opened_paragraph:
                opened_paragraph = False
                html_lines.append('</p>')
                
        # finally add this processed line to parsed html lines
        html_lines.append(line)
            
        # mark this line's type as previous for next line
        prev_line_was_empty = this_is_empty_line
    
    return html_lines




# Directory util functions

def dir_is_valid(dir_path_str):
    if not os.path.exists(dir_path_str):
        print("Error:", dir_path_str, "doesn't exist!")
        return False
    if not os.path.isdir(dir_path_str):
        print("Error:", dir_path_str, "isn't a directory!")
        return False
    return True

def make_dirnest_if_necessary(dir_path_str):
    directory = os.path.dirname(dir_path_str)
    if not os.path.exists(directory):
        os.makedirs(directory)
    

# Run main
if __name__ == "__main__":
    main(sys.argv[1:])
    
