from ext_html import process



def test_processing(name, strarr):

    print('\n== [',name,'] ============================')

    for line in strarr:
        print('  IN] ', line)
    print(' ---/')

    html = process(strarr)

    for line in html:
        print(' OUT] ', line)
    print(' ---/')
        


# -------------------------------------------------------
test_processing('lists', """

The following is a list: 

-- first
-- second
-- third

Now unusual cases:

--no space between items
  -- indented line

-- an empty line between list items

[listclass]
-- a classed list
-- with two items

-- list 
  [nest]
  -- with a nested list class

Description of a list
-- and some
-- list items


""".splitlines())


# -------------------------------------------------------
test_processing('comments', """

% this is some comments
% and this is a comment too

hello, this is 100% a paragraph, not a comment

   % this should be an indented comment

""".splitlines())



# --------------------------------------------------------
test_processing('links', """

`this`{HELL} is a link to HELL
`this one`{./HERE} points ./HERE

`this`{} is an empty link

""".splitlines())




# --------------------------------------------------------
test_processing('paragraphs', """

this is a new paragraph

this is an another paragraph


a paragraph with two empty lines

one after it


[big]
three empty lines

[very big]
another paragraph
just an another line, no empty line
not even here

another paragraph
without empty lines

""".splitlines())


# -------------------------------------------------------
test_processing('raw_html', """

this is a paragraph < 1 line

<<<
 <div class='heyo'>
   some stuff here
 </div>
>>>

~~ <span>hello</span>


""".splitlines())


# ---------------------------------------------------------
test_processing('headings', """

### This is a heading

this is a paragraph after a heading

### This is a heading that's too close
...to the text block after it
this might be a problem

""".splitlines())
